<?php
session_start();
?>
<table border="1">
    <tr>
        <td>ID</td>
        <td>Title</td>
        <td>Name</td>
        <td>Action</td>
    </tr>
    <?php
    foreach ($_SESSION['allData'] as $ab => $item) {
        ?>
        <tr>
            <td><?php echo $ab + 1 ?></td>
            <td><?php echo $item['title'] ?></td>
            <td><?php echo $item['name'] ?></td>
            <td><a href="edit.php?id=<?php echo $ab ?>">Edit</a>|<a href="delete.php?id=<?php echo $ab ?>&title=<?php echo $item['title']?>&name=<?php echo $item['name']?>">Delete</a></td>
        </tr>
    <?php }

    ?>
</table>